terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.66.0"
    }
  }
}


provider "google" {
  credentials = var.srv_acc_key
  project     = var.project
  region      = var.region
  zone        = var.zone
}


resource "google_pubsub_topic" "comments" {
  name = "comments"
}


resource "google_spanner_instance" "pde-spanner" {
  config       = var.config_region
  display_name = var.spanner_instance_name
  name         = var.spanner_instance_name
  num_nodes    = 2
}


resource "google_spanner_database" "pde-database" {
  instance            = google_spanner_instance.pde-spanner.name
  name                = var.spanner_db_name
  ddl                 = [var.create_comments]
  deletion_protection = false
}

resource "google_storage_bucket" "pdespannerbucket" {
  name = "pdespannerbucket"
}

resource "google_storage_bucket_object" "archive" {
  name   = "TODO"
  bucket = google_storage_bucket.pdespannerbucket.name
  source = "C:\\Users\\Alex\\Desktop Folder\\Projects\\yt_comment_gen\\TODO"
}

resource "google_cloudfunctions_function" "consumer" {
  name        = "consumer"
  description = "comment consumer"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.pdespannerbucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/${var.project}/topics/comments"
  }
  entry_point = "comment_sentiment_and_store"
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.consumer.project
  region         = google_cloudfunctions_function.consumer.region
  cloud_function = google_cloudfunctions_function.consumer.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}