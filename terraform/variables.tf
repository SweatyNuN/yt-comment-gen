variable "project" {
  description = "Project name for GCP"
  type        = string
  default     = "pde-gcloud-training"
}

variable "region" {
  description = "GCP region variable"
  type        = string
  default     = "europe-west2"
}

variable "zone" {
  description = "GCP zone variable"
  type        = string
  default     = "europe-west2-a"
}


variable "config_region" {
  description = "Instance configuration"
  type        = string
  default     = "regional-europe-west2"
}


variable "srv_acc_key" {
  description = "json key file for service account"
  type        = string
}


variable "spanner_instance_name" {
  description = "Spanner instance name"
  type        = string
  default     = "pde-spanner"
}

variable "spanner_db_name" {
  description = "Spanner database name"
  type        = string
  default     = "youtube"
}


variable "deletion_protection" {
  type    = bool
  default = false
}