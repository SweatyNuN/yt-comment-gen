variable "create_comments" {
  type        = string
  description = "Comments table creation"
  default     = <<EOF
  CREATE TABLE comments (
	author_channel_id STRING(MAX) NOT NULL,
	author_display_name STRING(MAX) NOT NULL,
	like_count INT64 NOT NULL,
	published_at TIMESTAMP,
	text STRING(MAX) NOT NULL,
	video_id STRING(MAX) NOT NULL,
	comment_id STRING(MAX) NOT NULL,
	score FLOAT64 NOT NULL,
    magnitude INT64 NOT NULL,
    ) PRIMARY KEY (comment_id)
  EOF
}