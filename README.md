# yt-comment-gen

Pipeline to fetch youtube comments and store in gcloud spanner and feed into GPT2 model for generation

## Folder structure

### config (not committed)

Contains gcloud_conf.yml to store thingies. Structure of the file below:

    ```yaml
    database:
    instance_id: "pde-spanner"
    database_id: "youtube"
    api:
    youtube:
        key: "api key goes here"
        version: "v3"
        service_name: "youtube"
    environment:
    GOOGLE_APPLICATION_CREDENTIALS: "file name of gcloud json key for signing in"
    OAUTHLIB_INSECURE_TRANSPORT: "1"
    pubsub:
    project_name: "pde-gcloud-training"
    topic_name: "comments"
    ```

Contains gcloud json key for signing in. Called whatever its called

### experiments

Contains earlier experiments when developing the pipeline

### model

Contains the code for training the model, generating responses from a trained model, and a gen folder for results of various experiments

### pipeline

Contains the code for fetching comments from videos and the code to deploy on the google cloud function

### Model experiments

Experiment 01: training the on one comment at a time and generating a result, model reset after each comment