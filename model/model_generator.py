import gpt_2_simple as gpt2

model_name = "124M"
sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess)
single_text = gpt2.generate(
    sess, 
    return_as_list=True,
    prefix='<|startoftext|> the will of God',
    truncate='<|endoftext|>',
    include_prefix=False,
    run_name='run1'
    )[0]
# TODO log source data better with comment
p = open(f"model/gen/comment.txt", "w", encoding="utf-8")
p.write(single_text)
p.close()