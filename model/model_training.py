import os
from google.cloud import spanner
import gpt_2_simple as gpt2
from pipeline.config_loader import load_gcloud_conf, set_gloud_creds


config = load_gcloud_conf()
set_gloud_creds(config)

instance_id = config.get("database").get("instance_id")
database_id = config.get("database").get("database_id")

#Spanner
client = spanner.Client()
instance = client.instance(instance_id)
database = instance.database(database_id)

def do_bot():
    model_name = "124M"
    if not os.path.isdir(os.path.join("models", model_name)):
        print(f"Downloading {model_name} model...")
        gpt2.download_gpt2(model_name=model_name)   # model is saved into current directory under /models/124M/

    with database.snapshot() as snapshot:
        results = snapshot.execute_sql("select * from comments order by (score * magnitude) asc LIMIT 1000;")

        with open("training_data.txt", "a", encoding='utf-8') as f:
            for row in results:
                f.write('<|startoftext|>' + row[4] + '<|endoftext|>' + '\n')

        sess = gpt2.start_tf_sess()
        gpt2.finetune(
                        sess,
                        'training_data.txt',
                        model_name=model_name,
                        steps=100
                    )
        

if __name__ == '__main__':
    do_bot()