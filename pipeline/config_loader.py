from yaml import safe_load
import os


def load_gcloud_conf(local=True):
    config_path = os.path.abspath(_get_dir(local))
    with open(config_path) as file:
        return safe_load(file)


def set_gloud_creds(config):
    creds_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "config",
            config.get("environment").get("GOOGLE_APPLICATION_CREDENTIALS"),
        )
    )
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = creds_path
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = config.get("environment").get(
        "OAUTHLIB_INSECURE_TRANSPORT"
    )


def _get_dir(local):
    if local is True:
        return os.path.join(
            os.path.dirname(__file__), "..", "config", "gcloud_conf.yml"
        )
    else:
        return os.path.join(
            os.path.dirname(__file__), "gcloud_conf.yml"
        )
