import os

import googleapiclient.discovery
from google.cloud import spanner
from google.cloud import language_v1
from google.cloud import pubsub_v1
import multiprocessing
from json import dumps
from retry import retry
from config_loader import load_gcloud_conf, set_gloud_creds


config = load_gcloud_conf()
set_gloud_creds(config)

# Pubsub
publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(
    config.get("pubsub").get("project_name"), config.get("pubsub").get("topic_name")
)

# Youtube 
yt_config = config.get("api").get("youtube")
api_service_name = yt_config.get("service_name")
api_version = yt_config.get("version")
DEVELOPER_KEY = yt_config.get("key")

youtube = googleapiclient.discovery.build(
    api_service_name, api_version, developerKey=DEVELOPER_KEY
)


def process_videos(job_id, youtube_id):
    """Logs youtube id and job id and kicks off fetch comment page

    Args:
        job_id (String): python process id
        youtube_id (List[String]): List of strings of youtube ids
    """
    print(f"Job ID started: {job_id} {youtube_id}")
    fetch_comment_page(youtube_id)


@retry(Exception, tries=3, delay=2)
def fetch_comment_page(youtube_id):
    for video in youtube_id:
        request = youtube.commentThreads().list(
            part="snippet", videoId=video, maxResults=100
        )
        response = request.execute()
        publish_message(response)

        while "nextPageToken" in response:
            next_page_id = response.get("nextPageToken")
            request = youtube.commentThreads().list(
                part="snippet", videoId=video, pageToken=next_page_id, maxResults=100
            )
            response = request.execute()
            publish_message(response)


def publish_message(comments):
    for item in comments.get("items"):
        comment_array = item.get("snippet").get("topLevelComment").get("snippet")
        channel_id = comment_array.get("authorChannelId").get("value")
        display_name = comment_array.get("authorDisplayName")
        like_count = comment_array.get("likeCount")
        published_at = comment_array.get("publishedAt")
        text = comment_array.get("textOriginal")
        video_id = comment_array.get("videoId")
        comment_id = item.get("snippet").get("topLevelComment").get("id")

        comment = {
            "channel_id": channel_id,
            "display_name": display_name,
            "like_count": like_count,
            "published_at": published_at,
            "text": text,
            "video_id": video_id,
            "comment_id": comment_id,
        }

        json_string = dumps(comment)

        data = json_string.encode("utf-8")
        # Add two attributes, origin and username, to the message
        future = publisher.publish(topic_path, data)
        print(future.result())


def dispatch_jobs(data, job_number):
    total = len(data)
    chunk_size = total // job_number
    slice = chunks(data, chunk_size)
    jobs = []

    for i, s in enumerate(slice):
        j = multiprocessing.Process(target=process_videos, args=(i, s))
        jobs.append(j)
    for j in jobs:
        j.start()


def chunks(l, n):
    return [l[i : i + n] for i in range(0, len(l), n)]


if __name__ == "__main__":
    dispatch_jobs(
        [
            "brOV89fNI_E"
        ], 1
    )
