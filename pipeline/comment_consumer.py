import base64
from json import loads
from google.cloud import spanner
from google.cloud import language_v1
from config_loader import load_gcloud_conf
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

config = load_gcloud_conf(local=False)

instance_id = config.get("database").get("instance_id")
database_id = config.get("database").get("database_id")

#Spanner
client = spanner.Client()
instance = client.instance(instance_id)
database = instance.database(database_id)

#Language api
#language_api = language_v1.LanguageServiceClient()


def comment_sentiment_and_store(event, context):
    """Triggered from a message on a Cloud Pub/Sub topic. Takes a youtube comment and gets the sentiment and stores it

    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    
    comment_dict = loads(pubsub_message)

    channel_id = comment_dict.get("channel_id")
    display_name = comment_dict.get("display_name")
    like_count = comment_dict.get("like_count")
    published_at = comment_dict.get("published_at")
    text = comment_dict.get("text")
    video_id = comment_dict.get("video_id")
    comment_id = comment_dict.get("comment_id")

    with database.snapshot() as db:
        result = db.execute_sql(f"SELECT * FROM comments WHERE comment_id='{comment_id}'").one_or_none()

        if result is None:
            #cdoc = language_v1.Document(content=text, type_=language_v1.Document.Type.PLAIN_TEXT)
            analyzer = SentimentIntensityAnalyzer()
            vs = analyzer.polarity_scores(text)

            #annotations = language_api.analyze_sentiment(request={'document': cdoc})
            score = vs.get("compound")
            magnitude = len(text)

            with database.batch() as b:
                print(f"Writing comment to table for video {video_id}")
                b.insert(
                    'comments',
                    columns=[
                        'author_channel_id',
                        'author_display_name',
                        'like_count',
                        'published_at',
                        'text',
                        'video_id',
                        'comment_id',
                        'score',
                        'magnitude'
                    ],
                    values=[
                        [
                            channel_id,
                            display_name,
                            like_count,
                            published_at,
                            text,
                            video_id,
                            comment_id,
                            score,
                            magnitude
                        ]
                    ],
                )