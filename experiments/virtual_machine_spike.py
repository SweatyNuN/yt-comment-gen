# -*- coding: utf-8 -*-

# Sample Python code for youtube.comments.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/guides/code_samples#python

import os

import googleapiclient.discovery
from google.cloud import spanner
from google.cloud import language_v1
import multiprocessing

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = ''
instance_id = 'pde-spanner'
database_id = 'youtube'

client = spanner.Client()
instance = client.instance(instance_id)
database = instance.database(database_id)
iamdatasciencewizard = language_v1.LanguageServiceClient()
types = spanner.param_types

def youtube(job_id, youtube_id):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    print(f"Job ID stooted: {job_id} {youtube_id}")
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = ""

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey = DEVELOPER_KEY
    )

    for video in youtube_id:
        request = youtube.commentThreads().list(
            part="snippet",
            videoId=video,
            maxResults=100
        )
        response = request.execute()
        write_to_table(response)

        while "nextPageToken" in response:
            next_page_id = response.get("nextPageToken")
            request = youtube.commentThreads().list(
                part="snippet",
                videoId=video,
                pageToken=next_page_id,
                maxResults=100
            )
            response = request.execute()
            write_to_table(response)


def write_to_table(response):

    comments = response

    for item in comments.get("items"):
        comment_array = item.get("snippet").get("topLevelComment").get("snippet")
        channel_id = comment_array.get("authorChannelId").get("value")
        display_name = comment_array.get("authorDisplayName")
        like_count = comment_array.get("likeCount")
        published_at = comment_array.get("publishedAt")
        text = comment_array.get("textOriginal")
        video_id = comment_array.get("videoId")
        comment_id = item.get("snippet").get("topLevelComment").get("id")

        with database.snapshot() as ooo:
            result = ooo.execute_sql(f"SELECT * FROM comments WHERE comment_id='{comment_id}'").one_or_none()

            if result is None:
                cdoc = language_v1.Document(content=text, type_=language_v1.Document.Type.PLAIN_TEXT)
                annotations = iamdatasciencewizard.analyze_sentiment(request={'document': cdoc})
                score = annotations.document_sentiment.score
                magnitude = annotations.document_sentiment.magnitude

                with database.batch() as b:
                    print(f"Writing comment to table for video {video_id}")
                    b.insert(
                        'comments',
                        columns=[
                            'author_channel_id',
                            'author_display_name',
                            'like_count',
                            'published_at',
                            'text',
                            'video_id',
                            'comment_id',
                            'score',
                            'magnitude'
                        ],
                        values=[
                            [
                                channel_id,
                                display_name,
                                like_count,
                                published_at,
                                text,
                                video_id,
                                comment_id,
                                score,
                                magnitude
                            ]
                        ],
                    )


def dispatch_jobs(data, job_number):
    total = len(data)
    chunk_size = total // job_number
    slice = chunks(data, chunk_size)
    jobs = []

    for i, s in enumerate(slice):
        j = multiprocessing.Process(target=youtube, args=(i, s))
        jobs.append(j)
    for j in jobs:
        j.start()

def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

if __name__ == '__main__':
    dispatch_jobs(["39-S9-ZXzqA", "dgT5J4L8dFQ", "WdrbuIm6yoo"], 3)

#  Primary Key(s): comment_id (asc)

# 	author_channel_id 	STRING(MAX) 	No 	— 	— 	
# 	author_display_name 	STRING(MAX) 	No 	— 	— 	
# 	like_count 	INT64 	No 	— 	— 	
# 	published_at 	TIMESTAMP 	No 	— 	— 	
# 	text 	STRING(MAX) 	No 	— 	— 	
# 	video_id 	STRING(MAX) 	No 	— 	— 	
# 	comment_id 	STRING(MAX) 	No 	— 	asc 	
# 	score 	STRING(MAX) 	No 	— 	— 	
# 	magnitude 	STRING(MAX) 	No 	— 	— 	

# {
#     "youtube_id": "OpjtHie-ZBQ"
# }

# CREATE TABLE comments (
# 	author_channel_id STRING(MAX) NOT NULL,
# 	author_display_name STRING(MAX) NOT NULL,
# 	like_count INT64 NOT NULL,
# 	published_at TIMESTAMP,
# 	text STRING(MAX) NOT NULL,
# 	video_id STRING(MAX) NOT NULL,
# 	comment_id STRING(MAX) NOT NULL,
# 	score FLOAT64 NOT NULL,
# 	magnitude FLOAT64 NOT NULL,
# ) PRIMARY KEY (comment_id)

# sudo add-apt-repository ppa:deadsnakes/ppa
# sudo apt install python3.6
# sudo apt-get remove python3-pip
# curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
# alias pip='/home/Alex/.local/bin/pip'