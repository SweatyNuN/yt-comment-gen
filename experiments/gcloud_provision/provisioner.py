from pipeline.config_loader import load_gcloud_conf, set_gloud_creds
from spanner import create_instance, create_database
from pubsub import create_pubsub_topic
from functions import create_function
import os

config = load_gcloud_conf()
set_gloud_creds(config)

ddl_path = os.path.join(
            os.path.dirname(__file__), "db_schema.sql"
        )

with open(ddl_path) as butt:
    ddl = butt.read()

db_conf = config.get("database")
create_instance(db_conf.get("instance_id"), db_conf.get("region"))
create_database(db_conf.get("instance_id"), db_conf.get("database_id"), ddl)

pubsub_conf = config.get("pubsub")
create_pubsub_topic(pubsub_conf.get("topic_name"), pubsub_conf.get("project_name"))

func_config = config.get("function")
create_function(
    func_config.get("folder"), 
    func_config.get("name"), 
    func_config.get("runtime"), 
    pubsub_conf.get("topic_name"), 
    pubsub_conf.get("project_name"), 
    func_config.get("max_instances"),
    func_config.get("file_name")
)
