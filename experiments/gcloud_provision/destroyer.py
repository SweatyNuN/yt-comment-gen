from pipeline.config_loader import load_gcloud_conf, set_gloud_creds
from spanner import delete_instance
from pubsub import delete_pubsub_topic
from functions import delete_function
import os

config = load_gcloud_conf()
set_gloud_creds(config)

ddl_path = os.path.join(
            os.path.dirname(__file__), "db_schema.sql"
        )

with open(ddl_path) as p:
    ddl = p.read()

db_conf = config.get("database")
delete_instance(db_conf.get("instance_id"))

pubsub_conf = config.get("pubsub")
delete_pubsub_topic(pubsub_conf.get("topic_name"), pubsub_conf.get("project_name"))

func_conf = config.get("function")
delete_function(func_conf.get("name"))