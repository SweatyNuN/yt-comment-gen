from google.cloud import pubsub_v1


def create_pubsub_topic(topic_name, project_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(
        project_name, topic_name
    )
    print("Creating topic")
    topic = publisher.create_topic(request={"name": topic_path})
    print("Topic created")


def delete_pubsub_topic(topic_name, project_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(
        project_name, topic_name
    )
    print("Deleting topic")
    topic = publisher.delete_topic(request={"topic": topic_path})
    print("gone bruh")