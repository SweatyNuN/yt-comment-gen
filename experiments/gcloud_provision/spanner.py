from google.cloud import spanner
from google.cloud.spanner_v1 import param_types


def create_instance(instance_id, region):
    """Creates an instance."""
    spanner_client = spanner.Client()

    config_name = f"{spanner_client.project_name}/instanceConfigs/{region}"

    instance = spanner_client.instance(
        instance_id,
        configuration_name=config_name,
        display_name="Youtube Comments",
        node_count=1,
    )

    operation = instance.create()

    print("Waiting for operation to complete...")
    operation.result(120)

    print("Created instance {}".format(instance_id))


def create_database(instance_id, database_id, ddl):
    """Creates a database and tables for sample data."""
    spanner_client = spanner.Client()
    instance = spanner_client.instance(instance_id)

    database = instance.database(
        database_id,
        ddl_statements=[
            ddl
        ],
    )

    operation = database.create()

    print("Waiting for operation to complete...")
    operation.result(120)

    print("Created database {} on instance {}".format(database_id, instance_id))


def delete_instance(instance_id):
    spanner_client = spanner.Client()
    instance = spanner_client.instance(
        instance_id
    )
    operation = instance.delete()
    print("gone bro")
