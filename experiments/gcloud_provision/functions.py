from subprocess import run
import shutil
import os


def create_function(function_folder, function_name, runtime, topic_name, project_id, max_instances, file_name):
    create_deploy_folder(function_folder, file_name)
    function_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            function_folder
        )
    )

    result = run(
        f'gcloud functions deploy {function_name} --region=europe-west2 --runtime={runtime} --source="{function_path}" --trigger-topic={topic_name} --project={project_id} --max-instances={max_instances} --retry --entry-point=comment_sentiment_and_store', shell=True
    )
    print("stdout:", result.stdout)
    print("stderr:", result.stderr)
    delete_deploy_folder(function_folder)


def delete_function(function_name):
    result = run(
        f'gcloud functions delete {function_name} --region=europe-west2 --quiet', shell=True
    )
    print("stdout:", result.stdout)
    print("stderr:", result.stderr)


def create_deploy_folder(function_folder, function_name):
    root = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            ".."
        )
    )
    function_path = os.path.join(root, function_folder)
    os.mkdir(function_path)
    original_function = os.path.join(root, "pipeline", function_name)
    clone_function = os.path.join(function_path, "main.py")
    shutil.copy(original_function, clone_function)
    config_loader = os.path.join(root, "pipeline", "config_loader.py")
    conflig_loaderaer_2 = os.path.join(function_path, "config_loader.py")
    shutil.copy(config_loader, conflig_loaderaer_2)
    reqUIRmeEJNts = os.path.join(root, "pipeline", "requirements.txt")
    ooo = os.path.join(function_path, "requirements.txt")
    shutil.copy(reqUIRmeEJNts, ooo)
    config_yoomle = os.path.join(root, "config", "gcloud_conf.yml")
    config_yoomle_2_2_2 = os.path.join(function_path, "gcloud_conf.yml")
    shutil.copy(config_yoomle, config_yoomle_2_2_2)


def delete_deploy_folder(function_folder):
    beemovie = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            function_folder
        )
    )
    shutil.rmtree(beemovie)