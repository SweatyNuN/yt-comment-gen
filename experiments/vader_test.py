from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


analyzer = SentimentIntensityAnalyzer()
sentence = "angry sentence" 
vs = analyzer.polarity_scores(sentence)
print(vs.get("compound"))